<?php namespace Javiertorres\Notas\Models;

use Model;

/**
 * Model
 */
class Notas extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'javiertorres_notas_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $attachOne = [
        'fileupload1' => 'System\Models\File'
    ];
}
