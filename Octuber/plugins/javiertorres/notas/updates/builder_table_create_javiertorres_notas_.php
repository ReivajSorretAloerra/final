<?php namespace Javiertorres\Notas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJaviertorresNotas extends Migration
{
    public function up()
    {
        Schema::create('javiertorres_notas_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->integer('titulo')->nullable();
            $table->text('descripcion')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('javiertorres_notas_');
    }
}
