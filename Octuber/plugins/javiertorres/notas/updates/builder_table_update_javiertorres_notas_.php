<?php namespace Javiertorres\Notas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJaviertorresNotas extends Migration
{
    public function up()
    {
        Schema::table('javiertorres_notas_', function($table)
        {
            $table->string('titulo', 10)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('javiertorres_notas_', function($table)
        {
            $table->integer('titulo')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
