<?php namespace Javiertorres\Notas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJaviertorresNotas2 extends Migration
{
    public function up()
    {
        Schema::table('javiertorres_notas_', function($table)
        {
            $table->string('titulo', 252)->change();
        });
    }
    
    public function down()
    {
        Schema::table('javiertorres_notas_', function($table)
        {
            $table->string('titulo', 10)->change();
        });
    }
}
