<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/partials/Footer.htm */
class __TwigTemplate_582d371b3eb86325c4456b1f679ed2b7ce90c9aa9bf115601b793f7706b6e20b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <title>Document</title>
  <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
</head>
<body>
  <!--Comienza la seccion del footer (Recordar que se debe de cambiar a un parcial dentro de cms)-->

<footer class=\"site-footer\" role=\"contentinfo\">
  <div class=\"container\">
    <div class=\"row mb-5\">
      <div class=\"col-md-4 mb-5\">
        <h3>Sobre la raza</h3>
        <p class=\"mb-5\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
        <ul class=\"list-unstyled footer-link d-flex footer-social\">
          <li><a href=\"#\" class=\"p-2\"><i class=\"fab fa-twitter\"></i></i></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-facebook\"></span></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-linkedin\"></span></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-instagram\"></span></a></li>
        </ul>

      </div>
      <div class=\"col-md-5 mb-5\">
        <h3>Contact Info</h3>
        <ul class=\"list-unstyled footer-link\">
          <li class=\"d-block\">
            <span class=\"d-block\">Direccion:</span>
            <span class=\"text-white\">Calle Tecamachalco , San Miguel Tecamachalco #109 , Naucalpan de juarez</span></li>
          <li class=\"d-block\"><span class=\"d-block\">Telefono:</span><span class=\"text-white\">+52 56-17-37-02-72</span></li>
          <li class=\"d-block\"><span class=\"d-block\">Email:</span><span class=\"text-white\">Javiercitotorres310@gmail.com</span></li>
        </ul>
      </div>
      <div class=\"col-md-3 mb-5\">
        <h3>Enlaces rapidos</h3>
        <ul class=\"list-unstyled footer-link\">
          <li><a href=\"#\">Acerca de</a></li>
          <li><a href=\"#\">Terminos de uso</a></li>
          <li><a href=\"#\">Quejas o sugerencias</a></li>
          <li><a href=\"#\">Contacto</a></li>
        </ul>
      </div>
      <div class=\"col-md-3\">
      
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-12 text-md-center text-left\">
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a></p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </div>
    </div>
  </div>
</footer>
<!-- END footer -->

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/partials/Footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <title>Document</title>
  <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
</head>
<body>
  <!--Comienza la seccion del footer (Recordar que se debe de cambiar a un parcial dentro de cms)-->

<footer class=\"site-footer\" role=\"contentinfo\">
  <div class=\"container\">
    <div class=\"row mb-5\">
      <div class=\"col-md-4 mb-5\">
        <h3>Sobre la raza</h3>
        <p class=\"mb-5\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
        <ul class=\"list-unstyled footer-link d-flex footer-social\">
          <li><a href=\"#\" class=\"p-2\"><i class=\"fab fa-twitter\"></i></i></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-facebook\"></span></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-linkedin\"></span></a></li>
          <li><a href=\"#\" class=\"p-2\"><span class=\"fa fa-instagram\"></span></a></li>
        </ul>

      </div>
      <div class=\"col-md-5 mb-5\">
        <h3>Contact Info</h3>
        <ul class=\"list-unstyled footer-link\">
          <li class=\"d-block\">
            <span class=\"d-block\">Direccion:</span>
            <span class=\"text-white\">Calle Tecamachalco , San Miguel Tecamachalco #109 , Naucalpan de juarez</span></li>
          <li class=\"d-block\"><span class=\"d-block\">Telefono:</span><span class=\"text-white\">+52 56-17-37-02-72</span></li>
          <li class=\"d-block\"><span class=\"d-block\">Email:</span><span class=\"text-white\">Javiercitotorres310@gmail.com</span></li>
        </ul>
      </div>
      <div class=\"col-md-3 mb-5\">
        <h3>Enlaces rapidos</h3>
        <ul class=\"list-unstyled footer-link\">
          <li><a href=\"#\">Acerca de</a></li>
          <li><a href=\"#\">Terminos de uso</a></li>
          <li><a href=\"#\">Quejas o sugerencias</a></li>
          <li><a href=\"#\">Contacto</a></li>
        </ul>
      </div>
      <div class=\"col-md-3\">
      
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-12 text-md-center text-left\">
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a></p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </div>
    </div>
  </div>
</footer>
<!-- END footer -->

</body>
</html>", "C:\\xampp\\htdocs\\Octuber/themes/razas/partials/Footer.htm", "");
    }
}
