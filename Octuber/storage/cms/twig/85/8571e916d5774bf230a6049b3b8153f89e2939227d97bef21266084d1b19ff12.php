<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/layouts/Razas.htm */
class __TwigTemplate_f8f815e6c4807c0dc016b698494104e65cb9c9048b25c95441cc04fa3d232ec1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/animate.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/owl.carousel.min.css");
        echo "\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/estilos.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/font-awesome.css");
        echo "\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/conjunto_perros.css");
        echo "\" rel=\"stylesheet\">
    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">
    <!-- Theme Style -->
    <link href=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\" rel=\"stylesheet\">
    <!--Css para las notas que yo agrego-->
    <link href=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/mis_notas.css");
        echo "\" rel=\"stylesheet\">

    <title>Document</title>
</head>
   <!--Header-->

   ";
        // line 31
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("Header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 32
        echo "   
   <!--Final del header-->
<body>

 

    <!--Contenido general de las pages(Paginas)-->

    <section >
        ";
        // line 41
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 42
        echo "    </section>

    <!--Hasta aqui llega el contenido de las pages-->








     <!--Footer-->

     ";
        // line 55
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("Footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 56
        echo "
     <!--Termina el footer-->

    
     <!--Comienzan los Scripts-->
     <script src=\"";
        // line 61
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/main.js");
        echo "\"></script>
     <script src=\"";
        // line 62
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/owl.carousel.min.js");
        echo "\"></script>
     <script src=\"";
        // line 63
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/jsjquery.waypoints.min.js");
        echo "\"></script>
     <script src=\"";
        // line 64
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/popper.min.js");
        echo "\"></script>
     <script src=\"";
        // line 65
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/bootstrap.min.js");
        echo "\"></script>
     <script src=\"";
        // line 66
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery-3.2.1.min.js");
        echo "\"></script> 
 <!--Hojas de javascript correspondientes al slider-->
 <script src=\"";
        // line 68
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/js/main.js");
        echo "\"></script>
 <!--Final de las hojas de javascript-->
 
     
 
     ";
        // line 73
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 74
        echo "</body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/layouts/Razas.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 74,  169 => 73,  161 => 68,  156 => 66,  152 => 65,  148 => 64,  144 => 63,  140 => 62,  136 => 61,  129 => 56,  125 => 55,  110 => 42,  108 => 41,  97 => 32,  93 => 31,  84 => 25,  79 => 23,  70 => 17,  64 => 14,  60 => 13,  55 => 11,  51 => 10,  47 => 9,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"{{ 'assets/css/animate.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/bootstrap.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/owl.carousel.min.css'|theme }}\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"{{ 'assets/slider/css/estilos.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/slider/css/font-awesome.css'|theme }}\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"{{ 'assets/slider/css/conjunto_perros.css'|theme }}\" rel=\"stylesheet\">
    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">
    <!-- Theme Style -->
    <link href=\"{{ 'assets/css/style.css'|theme }}\" rel=\"stylesheet\">
    <!--Css para las notas que yo agrego-->
    <link href=\"{{ 'assets/css/mis_notas.css'|theme }}\" rel=\"stylesheet\">

    <title>Document</title>
</head>
   <!--Header-->

   {% partial 'Header' %}
   
   <!--Final del header-->
<body>

 

    <!--Contenido general de las pages(Paginas)-->

    <section >
        {% page %}
    </section>

    <!--Hasta aqui llega el contenido de las pages-->








     <!--Footer-->

     {% partial 'Footer' %}

     <!--Termina el footer-->

    
     <!--Comienzan los Scripts-->
     <script src=\"{{ 'assets/js/main.js'|theme }}\"></script>
     <script src=\"{{ 'assets/js/owl.carousel.min.js'|theme }}\"></script>
     <script src=\"{{ 'assets/jsjquery.waypoints.min.js'|theme }}\"></script>
     <script src=\"{{ 'assets/js/popper.min.js'|theme }}\"></script>
     <script src=\"{{ 'assets/js/bootstrap.min.js'|theme }}\"></script>
     <script src=\"{{ 'assets/js/jquery-3.2.1.min.js'|theme }}\"></script> 
 <!--Hojas de javascript correspondientes al slider-->
 <script src=\"{{ 'assets/slider/js/main.js'|theme }}\"></script>
 <!--Final de las hojas de javascript-->
 
     
 
     {%scripts%}
</body>
</html>", "C:\\xampp\\htdocs\\Octuber/themes/razas/layouts/Razas.htm", "");
    }
}
