<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/pages/index.htm */
class __TwigTemplate_a1eab68bf7d9fd875860c9ebe239d41ce91f157cc9418bbea86d81bd043fc4e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
  <head>
    <title>DogBeautifull</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/animate.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/owl.carousel.min.css");
        echo "\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"";
        // line 14
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/estilos.css");
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/font-awesome.css");
        echo "\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/css/conjunto_perros.css");
        echo "\" rel=\"stylesheet\">

    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">

    <!-- Theme Style -->
    
    <link href=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\" rel=\"stylesheet\">
  </head>
  <br>
<br>
<br>
<!-- Hasta aqui es el codigo del slider -->

    <!--Empieza la seccion principal de la imagen-->

    <section class=\"section element-animate\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5\">
          <div class=\"col-md-4\"></div>
          <div class=\"col-md-8 section-heading\">
            <h2>It's a Dog Life</h2>
            <p class=\"small-sub-heading\">Curious story of Dogs</p>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-md-4 mb-4\">
            
            <img src=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p><a href=\"#\">Learn More <span class=\"ion-ios-arrow-right\"></span></a></p>
          </div>
        </div>
      </div>
    </section>
   
    <section class=\"section bg-light\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Cada perro necesita una buena dueña</h2>
            <p class=\"mb-5 lead\">Tienes la oportunidad de convertirte en la persona mas deseada para alguno de estos animales.Porque no hacerlo??</p>
          </div>
        </div>
        <!--Carrusel de imagenes personas-->
        <div class=\"row element-animate\">
          <div class=\"major-caousel js-carousel-1 owl-carousel\">
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 78
        echo $this->extensions['System\Twig\Extension']->mediaFilter("assets/slider/img/person_1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mellisa Howard</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 86
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/person_2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mike Richardson</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"#\"><img src=\"";
        // line 94
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/person_3.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Charles White</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"";
        // line 102
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/person_4.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Laura Smith</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
         <!--Final de carrusel de imagenes-->
      </div>
    </section>
    <!-- END section -->

    <section class=\"section border-t\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Colecciones de razas de perros</h2>
            <p class=\"mb-5 lead\">Ya estas aqui , Porque marcharte sin hecharle un ojo  a la galeria de algunos de nuestros animales?.Encontraras de distintas razas y tamaños.Vamos a ello!!</p>
          </div>
        </div>
<!--Imagenes de los perros en conjunto-->
        <div class=\"row no-gutters\">
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">German Shepherd</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"";
        // line 132
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_1.jpg");
        echo "\" alt=\"Image placeholder\" id=\"img\"   class=\"img-fluid\" /> 
              </a>  
            </div>
             
            
          </div>
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Labrador</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 145
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>

              </div>
              
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Bulldog</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 160
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_3.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
            </div>
          
         
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">Rottweiler</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"";
        // line 173
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_4.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
              </a>
            </div>
            
          </div>
         
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Beagle</h3>
                  </div>
                  <i class=\"far fa-plus\"></i>
                  <img src=\"";
        // line 187
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_5.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\" >
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Golden Retriever</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"";
        // line 201
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/dog_6.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
        </div>
        
      </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br> 
    <!-- END section -->

    <!--Comienza el blog-->
    
    <section class=\"section blog\">
      <div class=\"container\">

        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">
              Publicación reciente en el blog</h2>
            <p class=\"mb-5 lead\">La informacion mas reciente acerca de nuestra comunidad. </p>
          </div>
        </div>

        <div class=\"rownota\">
          <div class=\"col-md-6\">
            ";
        // line 233
        $context["records"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "records", [], "any", false, false, false, 233);
        // line 234
        echo "            ";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "displayColumn", [], "any", false, false, false, 234);
        // line 235
        echo "            ";
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "noRecordsMessage", [], "any", false, false, false, 235);
        // line 236
        echo "            ";
        $context["detailsPage"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsPage", [], "any", false, false, false, 236);
        // line 237
        echo "            ";
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsKeyColumn", [], "any", false, false, false, 237);
        // line 238
        echo "            ";
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->source, ($context["builderList"] ?? null), "detailsUrlParameter", [], "any", false, false, false, 238);
        // line 239
        echo "            
           
            
            
            <ul class=\"record-list\"> 



              <div class=\"media mb-4 d-md-flex d-block element-animate\">
                <div class=\"media-body\">
                ";
        // line 249
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 250
            echo "
             
              <span class=\"post-meta\"> ";
            // line 252
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "fecha", [], "any", false, false, false, 252), "html", null, true);
            echo "</span>
                  
               
               <h3 class=\"mt-2 text-black\">
                 
                ";
            // line 257
            ob_start();
            // line 258
            echo "                  
                    ";
            // line 259
            if (($context["detailsPage"] ?? null)) {
                // line 260
                echo "
                    <a href=\"";
                // line 261
                echo $this->extensions['Cms\Twig\Extension']->pageFilter(($context["detailsPage"] ?? null), [($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->source, $context["record"], ($context["detailsKeyColumn"] ?? null), [], "any", false, false, false, 261)]);
                echo "
                    
                    \">
                      ";
            }
            // line 265
            echo "
                      ";
            // line 266
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["record"], "titulo", [], "any", false, false, false, 266), "html", null, true);
            echo "

                   ";
            // line 268
            if (($context["detailsPage"] ?? null)) {
                echo "   

                    </a>

                    ";
            }
            // line 273
            echo "
                    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 275
            echo "              
                </h3>
              
              <br>
                   
              <img src=\"";
            // line 280
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["record"], "fileupload1", [], "any", false, false, false, 280), "path", [], "any", false, false, false, 280), "html", null, true);
            echo "\" alt=\"Placeholder image\" class=\"imgnota\">
                
                <br>
                <br>
              ";
            // line 284
            echo call_user_func_array($this->env->getFunction('html_limit')->getCallable(), ["limit", twig_get_attribute($this->env, $this->source, $context["record"], "descripcion", [], "any", false, false, false, 284), 150]);
            echo "
              
           <br>
           
                   
                 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 289
        echo "  
                </div> 
              </div>
            </ul>

           
        </div>
      </div>
    </section>
    <!--Termina la seccion del blog-->
     
    <!--Footer-->
    
    
    <!--Scripts-->
    <script src=\"";
        // line 304
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/main.js");
        echo "\"></script>
    <script src=\"";
        // line 305
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/owl.carousel.min.js");
        echo "\"></script>
    <script src=\"";
        // line 306
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/jsjquery.waypoints.min.js");
        echo "\"></script>
    <script src=\"";
        // line 307
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/popper.min.js");
        echo "\"></script>
    <script src=\"";
        // line 308
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/bootstrap.min.js");
        echo "\"></script>
    <script src=\"";
        // line 309
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery-3.2.1.min.js");
        echo "\"></script>
<!--Hojas de javascript correspondientes al slider-->
<script src=\"";
        // line 311
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/js/main.js");
        echo "\"></script>
<!--Final de las hojas de javascript-->

    

    ";
        // line 316
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 317
        echo "  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  486 => 317,  483 => 316,  475 => 311,  470 => 309,  466 => 308,  462 => 307,  458 => 306,  454 => 305,  450 => 304,  433 => 289,  421 => 284,  414 => 280,  407 => 275,  403 => 273,  395 => 268,  390 => 266,  387 => 265,  380 => 261,  377 => 260,  375 => 259,  372 => 258,  370 => 257,  362 => 252,  358 => 250,  354 => 249,  342 => 239,  339 => 238,  336 => 237,  333 => 236,  330 => 235,  327 => 234,  325 => 233,  290 => 201,  273 => 187,  256 => 173,  240 => 160,  222 => 145,  206 => 132,  173 => 102,  162 => 94,  151 => 86,  140 => 78,  108 => 49,  84 => 28,  71 => 18,  65 => 15,  61 => 14,  56 => 12,  52 => 11,  48 => 10,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
  <head>
    <title>DogBeautifull</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\" crossorigin=\"anonymous\"></script>
    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800\" rel=\"stylesheet\">
    <script src=\"https://kit.fontawesome.com/d8d67c135a.js\"></script>
    <link href=\"{{ 'assets/css/animate.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/bootstrap.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/css/owl.carousel.min.css'|theme }}\" rel=\"stylesheet\">
    <!--Las hojas de estilos son pertenecientes al slider-->
    <link href=\"{{ 'assets/slider/css/estilos.css'|theme }}\" rel=\"stylesheet\">
    <link href=\"{{ 'assets/slider/css/font-awesome.css'|theme }}\" rel=\"stylesheet\">
    <!--Hasta aqui llegan las hojas de estilo del slider-->
    <!--Comiezan Hojas de estilos para el principal conjunto de imagenes de perros-->
    <link href=\"{{ 'assets/slider/css/conjunto_perros.css'|theme }}\" rel=\"stylesheet\">

    <!--Hasta aqui termina la seccion de hojas de estilo para la parte principal de las imagenes de los perros-->
    <link rel=\"stylesheet\" href=\"assets/fonts/ionicons/css/ionicons.min.css\">
    <link rel=\"stylesheet\" href=\"fonts/fontawesome/css/font-awesome.min.css\">

    <link rel=\"stylesheet\" href=\"fonts/flaticon/font/flaticon.css\">

    <!-- Theme Style -->
    
    <link href=\"{{ 'assets/css/style.css'|theme }}\" rel=\"stylesheet\">
  </head>
  <br>
<br>
<br>
<!-- Hasta aqui es el codigo del slider -->

    <!--Empieza la seccion principal de la imagen-->

    <section class=\"section element-animate\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5\">
          <div class=\"col-md-4\"></div>
          <div class=\"col-md-8 section-heading\">
            <h2>It's a Dog Life</h2>
            <p class=\"small-sub-heading\">Curious story of Dogs</p>
          </div>
        </div>
        <div class=\"row\">
          <div class=\"col-md-4 mb-4\">
            
            <img src=\"{{ 'assets/img/dog_1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
          </div>
          <div class=\"col-md-4\">
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <p><a href=\"#\">Learn More <span class=\"ion-ios-arrow-right\"></span></a></p>
          </div>
        </div>
      </div>
    </section>
   
    <section class=\"section bg-light\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Cada perro necesita una buena dueña</h2>
            <p class=\"mb-5 lead\">Tienes la oportunidad de convertirte en la persona mas deseada para alguno de estos animales.Porque no hacerlo??</p>
          </div>
        </div>
        <!--Carrusel de imagenes personas-->
        <div class=\"row element-animate\">
          <div class=\"major-caousel js-carousel-1 owl-carousel\">
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/slider/img/person_1.jpg'|media }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mellisa Howard</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/slider/img/person_2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Mike Richardson</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"#\"><img src=\"{{ 'assets/img/person_3.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Charles White</h3>
                </div>
              </div>
            </div>
            <div>
              <div class=\"media d-block media-custom text-center\">
                <a href=\"adoption-single.html\"><img src=\"{{ 'assets/img/person_4.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" /></a>
                <div class=\"media-body\">
                  <h3 class=\"mt-0 text-black\">Laura Smith</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
         <!--Final de carrusel de imagenes-->
      </div>
    </section>
    <!-- END section -->

    <section class=\"section border-t\">
      <div class=\"container\">
        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">Colecciones de razas de perros</h2>
            <p class=\"mb-5 lead\">Ya estas aqui , Porque marcharte sin hecharle un ojo  a la galeria de algunos de nuestros animales?.Encontraras de distintas razas y tamaños.Vamos a ello!!</p>
          </div>
        </div>
<!--Imagenes de los perros en conjunto-->
        <div class=\"row no-gutters\">
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">German Shepherd</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"{{ 'assets/img/dog_1.jpg'|theme }}\" alt=\"Image placeholder\" id=\"img\"   class=\"img-fluid\" /> 
              </a>  
            </div>
             
            
          </div>
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Labrador</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>

              </div>
              
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Bulldog</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_3.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
            </div>
          
         
          <div class=\"col-md-4 element-animate\">
            <div class=\"c-img\">
              <a href=\"single.html\" class=\"link-thumbnail\">
                <div class=\"txt\">
                  <h3 id=\"h3\">Rottweiler</h3>
                </div>
                <span class=\"ion-plus icon\"></span>
                <img src=\"{{ 'assets/img/dog_4.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
              </a>
            </div>
            
          </div>
         
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\">
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Beagle</h3>
                  </div>
                  <i class=\"far fa-plus\"></i>
                  <img src=\"{{ 'assets/img/dog_5.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
          
            <div class=\"col-md-4 element-animate\">
              <div class=\"c-img\" >
                <a href=\"single.html\" class=\"link-thumbnail\">
                  <div class=\"txt\">
                    <h3 id=\"h3\">Golden Retriever</h3>
                  </div>
                  <span class=\"ion-plus icon\"></span>
                  <img src=\"{{ 'assets/img/dog_6.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
                </a>
              </div>
             
            </div>
          
        </div>
        
      </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br> 
    <!-- END section -->

    <!--Comienza el blog-->
    
    <section class=\"section blog\">
      <div class=\"container\">

        <div class=\"row justify-content-center mb-5 element-animate\">
          <div class=\"col-md-8 text-center\">
            <h2 class=\" heading mb-4\">
              Publicación reciente en el blog</h2>
            <p class=\"mb-5 lead\">La informacion mas reciente acerca de nuestra comunidad. </p>
          </div>
        </div>

        <div class=\"rownota\">
          <div class=\"col-md-6\">
            {% set records = builderList.records %}
            {% set displayColumn = builderList.displayColumn %}
            {% set noRecordsMessage = builderList.noRecordsMessage %}
            {% set detailsPage = builderList.detailsPage %}
            {% set detailsKeyColumn = builderList.detailsKeyColumn %}
            {% set detailsUrlParameter = builderList.detailsUrlParameter %}
            
           
            
            
            <ul class=\"record-list\"> 



              <div class=\"media mb-4 d-md-flex d-block element-animate\">
                <div class=\"media-body\">
                {% for record in records %}

             
              <span class=\"post-meta\"> {{record.fecha}}</span>
                  
               
               <h3 class=\"mt-2 text-black\">
                 
                {% spaceless %}
                  
                    {% if detailsPage %}

                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record,detailsKeyColumn) }) }}
                    
                    \">
                      {% endif %}

                      {{record.titulo}}

                   {% if detailsPage %}   

                    </a>

                    {% endif %}

                    {% endspaceless %}
              
                </h3>
              
              <br>
                   
              <img src=\"{{record.fileupload1.path}}\" alt=\"Placeholder image\" class=\"imgnota\">
                
                <br>
                <br>
              {{html_limit(record.descripcion,150)|raw}}
              
           <br>
           
                   
                 {% endfor %}  
                </div> 
              </div>
            </ul>

           
        </div>
      </div>
    </section>
    <!--Termina la seccion del blog-->
     
    <!--Footer-->
    
    
    <!--Scripts-->
    <script src=\"{{ 'assets/js/main.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/owl.carousel.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/jsjquery.waypoints.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/popper.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/bootstrap.min.js'|theme }}\"></script>
    <script src=\"{{ 'assets/js/jquery-3.2.1.min.js'|theme }}\"></script>
<!--Hojas de javascript correspondientes al slider-->
<script src=\"{{ 'assets/slider/js/main.js'|theme }}\"></script>
<!--Final de las hojas de javascript-->

    

    {%scripts%}
  </body>
</html>", "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/index.htm", "");
    }
}
