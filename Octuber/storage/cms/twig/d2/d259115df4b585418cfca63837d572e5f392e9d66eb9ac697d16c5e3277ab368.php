<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/pages/nota.htm */
class __TwigTemplate_1cfcab78ce7f616509ce98a0ac0a84d35d8a593b38bd8ea1991ba284735e0993 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["record"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "record", [], "any", false, false, false, 1);
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "displayColumn", [], "any", false, false, false, 2);
        // line 3
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->source, ($context["builderDetails"] ?? null), "notFoundMessage", [], "any", false, false, false, 3);
        // line 4
        echo "<div class=\"impresion\">

    ";
        // line 6
        if (($context["record"] ?? null)) {
            // line 7
            echo "    <h3 class=\"titulonota\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), ($context["displayColumn"] ?? null), [], "any", false, false, false, 7), "html", null, true);
            echo "</h3>
    

</div>
<div class=\"impcuerpo\">
    ";
            // line 12
            echo twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "descripcion", [], "any", false, false, false, 12);
            echo "
</div>
<div class=\"imgdnota\">
    <img src=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["record"] ?? null), "fileupload1", [], "any", false, false, false, 15), "path", [], "any", false, false, false, 15), "html", null, true);
            echo "\" alt=\"Placeholder image\">
</div>

";
        } else {
            // line 19
            echo "    ";
            echo twig_escape_filter($this->env, ($context["notFoundMessage"] ?? null), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/nota.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 19,  64 => 15,  58 => 12,  49 => 7,  47 => 6,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}
<div class=\"impresion\">

    {% if record %}
    <h3 class=\"titulonota\">{{ attribute(record, displayColumn) }}</h3>
    

</div>
<div class=\"impcuerpo\">
    {{record.descripcion|raw}}
</div>
<div class=\"imgdnota\">
    <img src=\"{{record.fileupload1.path}}\" alt=\"Placeholder image\">
</div>

{% else %}
    {{ notFoundMessage }}
{% endif %}", "C:\\xampp\\htdocs\\Octuber/themes/razas/pages/nota.htm", "");
    }
}
