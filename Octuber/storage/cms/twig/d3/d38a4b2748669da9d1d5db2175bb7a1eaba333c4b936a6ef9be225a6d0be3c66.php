<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\Octuber/themes/razas/partials/Header.htm */
class __TwigTemplate_ecde8da1140c9eaecec3c397f77a79379a07f0f0bedf71c57488af4f6ad4787e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <title>Document</title>
</head>

<header role=\"banner\" id=\"header\">
  <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
    <div class=\"container\">
      <a class=\"navbar-brand absolute\" href=\"index.html\">breed</a>
      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample05\" aria-controls=\"navbarsExample05\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
      </button>

      <div class=\"collapse navbar-collapse\" id=\"navbarsExample05\">
        <ul class=\"navbar-nav mx-auto pl-lg-5 pl-0\">
          <li class=\"nav-item\">
            <a class=\"nav-link active\" href=\"index.html\">Home</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"about.html\">About</a>
          </li>
          <li class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle\" href=\"services.html\" id=\"dropdown04\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Breed</a>
            <div class=\"dropdown-menu\" aria-labelledby=\"dropdown04\">
              <a class=\"dropdown-item\" href=\"#\">German Shepherd</a>
              <a class=\"dropdown-item\" href=\"#\">Labrador</a>
              <a class=\"dropdown-item\" href=\"#\">Rottweiler</a>
            </div>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"blog.html\">Blog</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"contact.html\">Contact</a>
          </li>
        </ul>
        
      </div>
    </div>
  </nav>
</header>
<!-- END header -->
<!--slider-->
<body>
<!--Carrusel de Imagenes-->

<div class=\"slideshow\">
<ul class=\"slider\">

<li>
<img src=\"";
        // line 54
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/slider-1.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />
<section class=\"caption\">
 <h1 class=\"titulo_slider\">AMAMOS A LAS MASCOTAS.</h1>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
  <br>
  <div class=\"btn-1\">
    <input type=\"submit\" value=\"Get Started\" class=\"btn\"><input type=\"submit\" value=\"Descargar\" class=\"btnn\">
  </div>
  </section>
  </li>
  <li>
<img src=\"";
        // line 65
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/slider/img/slider-2.jpg");
        echo "\" alt=\"Image placeholder\" class=\"img-fluid\" />          <section class=\"caption\">
<h1 class=\"titulo_slider\">CUIDAR PERROS</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
</section>



</li>

</ul>
<ol class=\"pagination\">
 
</ol>

<div class=\"left\">
    <span class=\"fa fa-chevron-left\"></span>
</div>

<div class=\"right\">
    <span class=\"fa fa-chevron-right\"></span>
</div>

</div>


<body>
  
  
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\Octuber/themes/razas/partials/Header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 65,  92 => 54,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <title>Document</title>
</head>

<header role=\"banner\" id=\"header\">
  <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
    <div class=\"container\">
      <a class=\"navbar-brand absolute\" href=\"index.html\">breed</a>
      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExample05\" aria-controls=\"navbarsExample05\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
      </button>

      <div class=\"collapse navbar-collapse\" id=\"navbarsExample05\">
        <ul class=\"navbar-nav mx-auto pl-lg-5 pl-0\">
          <li class=\"nav-item\">
            <a class=\"nav-link active\" href=\"index.html\">Home</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"about.html\">About</a>
          </li>
          <li class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle\" href=\"services.html\" id=\"dropdown04\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Breed</a>
            <div class=\"dropdown-menu\" aria-labelledby=\"dropdown04\">
              <a class=\"dropdown-item\" href=\"#\">German Shepherd</a>
              <a class=\"dropdown-item\" href=\"#\">Labrador</a>
              <a class=\"dropdown-item\" href=\"#\">Rottweiler</a>
            </div>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"blog.html\">Blog</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"contact.html\">Contact</a>
          </li>
        </ul>
        
      </div>
    </div>
  </nav>
</header>
<!-- END header -->
<!--slider-->
<body>
<!--Carrusel de Imagenes-->

<div class=\"slideshow\">
<ul class=\"slider\">

<li>
<img src=\"{{ 'assets/slider/img/slider-1.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />
<section class=\"caption\">
 <h1 class=\"titulo_slider\">AMAMOS A LAS MASCOTAS.</h1>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
  <br>
  <div class=\"btn-1\">
    <input type=\"submit\" value=\"Get Started\" class=\"btn\"><input type=\"submit\" value=\"Descargar\" class=\"btnn\">
  </div>
  </section>
  </li>
  <li>
<img src=\"{{ 'assets/slider/img/slider-2.jpg'|theme }}\" alt=\"Image placeholder\" class=\"img-fluid\" />          <section class=\"caption\">
<h1 class=\"titulo_slider\">CUIDAR PERROS</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci quis ipsa, id quidem quisquam unde.</p>
</section>



</li>

</ul>
<ol class=\"pagination\">
 
</ol>

<div class=\"left\">
    <span class=\"fa fa-chevron-left\"></span>
</div>

<div class=\"right\">
    <span class=\"fa fa-chevron-right\"></span>
</div>

</div>


<body>
  
  
</body>
</html>", "C:\\xampp\\htdocs\\Octuber/themes/razas/partials/Header.htm", "");
    }
}
